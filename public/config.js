window.REACT_CONFIG = {
    API_DEFAULT: 'http://localhost:9321/api',
    API_AUTH: 'http://localhost:9321/api/auth',
    DEFAULT_LANGUAGE: 'en',
    features: {
        message: true,
        marketingCampaign: true,
        analyze: true,
    }
};