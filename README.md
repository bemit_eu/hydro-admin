# Flood\Hydro: Admin

The user interface for [Hydro eCommerce CMS](https://bitbucket.org/bemit_eu/hydro-bucket), see [Hydro Bundle](https://bitbucket.org/bemit_eu/hydro-bundle) for development of admin and backend.

- [Config](#markdown-header-config)
- [Setup](#markdown-header-setup)
- [Docker Image](#markdown-header-docker-image)
- [Download Build](#markdown-header-download-build)

## Config

See `.env` for the api endpoints against which the app runs, `env` variables could be used when building the admin..

If you want to change the config after the build, don't use `env` but change the `config.js` within the builded files.

## Setup

```bash
git clone --recurse-submodules -j8 https://bitbucket.org/bemit_eu/hydro-admin.git

cd hydro-admin

# install deps
npm i
# and again, as submodules depth is too much for npm atm.
npm i

# start dev-server & file watcher
npm start
# localhost:9220

# create production build
npm run build
# see ./build folder, copy files into webspace
# for usage with Apache: copy files within server_apache into build
# for usage with NodeJS: copy files within server_nodejs into webspace 
```

## Docker Image

[Docker Hub](https://hub.docker.com/r/bemiteu/hydro-admin)

Creating a DockerImage with `server_nodejs` and `build`:

```bash
# Build Docker Local
# setup repo & install dependencies, then:
npm run build
cp ./server_nodejs/* ./build/

docker build -t <some-name> .

# run docker, `--rm` remove container after pressing e.g. `CTRL + C`, publish the port `9220` 
docker run -it --rm -p 9220:9220 <some-name> sh
# on windows use absolute path e.g. C:\Users will be //c/Users
docker run -it --rm -p 9220:9220 -v $(pwd)/config.js:/home/node/app/config.js <some-name> sh
```

### Pre-Build Images

```bash
# Releases
docker run -it --rm -p 9220:9220 bemiteu/hydro-admin:v0.4.2 sh

docker run -p 9220:9220 bemiteu/hydro-admin:v0.4.2
docker run -p 9220:9220 bemiteu/hydro-admin:v0.3.1


# Branches
docker run -p 9220:9220 bemiteu/hydro-admin:master
docker run -p 9220:9220 bemiteu/hydro-admin:master_<build-id>
docker run -p 9220:9220 bemiteu/hydro-admin:develop
docker run -p 9220:9220 bemiteu/hydro-admin:develop_<build-id>
```

## Download Build

- for Apache
    - releases:
        - 0.4.2, [hydro-admin_v0.4.2-apache.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/hydro-admin_v0.4.2-apache.zip)
        - 0.3.1, [hydro-admin_v0.3.1-apache.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/hydro-admin_v0.3.1-apache.zip)
    - latest [master_apache.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/master_apache.zip)
    - latest [develop_apache.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/develop_apache.zip)
- for NodeJS
    - releases:
        - 0.4.2, [hydro-admin_v0.4.2-nodejs.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/hydro-admin_v0.4.2-nodejs.zip)
        - 0.3.1, [hydro-admin_v0.3.1-nodejs.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/hydro-admin_v0.3.1-nodejs.zip)
    - latest [master_nodejs.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/master_nodejs.zip)
    - latest [develop_nodejs.zip](https://bitbucket.org/bemit_eu/hydro-admin/downloads/develop_nodejs.zip)

# Licence

This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of CeCILL-C (v1) for Europe or GNU LGPL (v3) for the rest of the world.

This file and the LICENCE.* files need to be distributed and not changed when distributing.
For more informations on the Licences which are applied read: [LICENCE.md](LICENCE.md)

# Copyright

2018 - 2019 | [bemit UG](https://bemit.eu) (haftungsbeschränkt) - project@bemit.codes

Maintained by [Michael Becker](https://mlbr.xyz)