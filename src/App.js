import React from 'react';
import Loadable from 'react-loadable';
import Loading from '@bemit/flood-admin/component/Loading';
import BaseApp from '@bemit/flood-admin/BaseApp';

import {addAlias, enableCustomDefault} from '@bemit/flood-admin/lib/i18n';

import {ReactComponent as Logo} from '@bemit/flood-admin/asset/logo.svg';
import {ReactComponent as LogoWhite} from '@bemit/flood-admin/asset/logo_white.svg';

enableCustomDefault('schema', 'article-meta');
enableCustomDefault('schema', 'doc-tree.block._content-common');
enableCustomDefault('schema', 'product.simple');
enableCustomDefault('schema', 'product.wine-product');
enableCustomDefault('schema', 'product.wine-bundle');
enableCustomDefault('doc-tree', 'selection');
enableCustomDefault('doc-tree', 'root');
enableCustomDefault('shop.product', 'types');

addAlias('schema', 'doc-tree.block.banner', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.btn-block', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.event', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.gallery', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.headline-bold', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.image', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.text', 'doc-tree.block._content-common');
addAlias('schema', 'doc-tree.block.text-image', 'doc-tree.block._content-common');
addAlias('schema', 'product.wine-bundle', 'product.wine-product');

function isUndefined(variable) {
    return 'undefined' === typeof variable;
}

let config = {};
if(window.REACT_CONFIG) {
    config = window.REACT_CONFIG;
} else {
    console.error('App started without config, exit.');
    throw new Error('App started without config, exit.');
}

// Setup Feature Toggles
const features = {
    message: (config && config.features && !isUndefined(config.features.message)) ? config.features.message : false,
    content: {
        'create-section': true,
        'update-section': true,
        'create-article': true
    },
    media: true,
    shop: true,
    marketing: true,
    marketingCampaign: (config && config.features && !isUndefined(config.features.marketingCampaign)) ? config.features.marketingCampaign : false,
    user: true,
    analyze: (config && config.features && !isUndefined(config.features.analyze)) ? config.features.analyze : false,
    block: false,
    connection: false,
    backendConnect: true,
    authPermission: true,
};

// overwrite config if env-vars exist
if(process.env.REACT_APP_API_DEFAULT) {
    config.API_DEFAULT = process.env.REACT_APP_API_DEFAULT;
}
if(process.env.REACT_APP_API_AUTH) {
    config.API_AUTH = process.env.REACT_APP_API_AUTH;
}
if(process.env.DEFAULT_LANGUAGE) {
    config.DEFAULT_LANGUAGE = process.env.REACT_APP_DEFAULT_LANGUAGE;
}

const api = {
    api: config.API_DEFAULT,
    file: config.API_DEFAULT,
    auth: config.API_AUTH
};

const routesComponents = {
    pages: {
        info: Loadable({
            loader: () => import('./page/Info'),
            loading: (props) => <Loading {...props} name='page/Info'/>,
        })
    }
};

const branding = {
    logo: {
        normal: Logo,
        white: LogoWhite,
    }
};

const locales = {
    en: {
        'common': {
            header: {
                slogan: 'Flood\\Hydro'
            }
        },
        'page--auth': {
            title: 'Hydro Admin'
        },
        'page--info': {
            title: 'Hydro: Admin + Editor',
            info: {
                contact: {
                    support: {
                        subject: "Request Technical Solutions [Hydro Admin]"
                    },
                    guide: {
                        subject: "Request User Support [Hydro Admin]"
                    },
                    law: {
                        subject: "Company Request [Hydro Admin]"
                    }
                }
            }
        }
    },
    de: {
        'common': {
            header: {
                slogan: 'Flood\\Hydro'
            }
        },
        'page--auth': {
            title: 'Hydro Admin'
        },
        'page--info': {
            title: 'Hydro: Admin + Editor',
            info: {
                contact: {
                    support: {
                        subject: "Anfrage Technische Lösung [Hydro Admin]"
                    },
                    guide: {
                        subject: "Anfrage Bedienungs Hilfe [Hydro Admin]"
                    },
                    law: {
                        subject: "Unternehmens Anfrage [Hydro Admin]"
                    }
                }
            }
        }
    }
};

class App extends React.Component {
    render() {
        return (
            <BaseApp api={api}
                     routesComponents={routesComponents}
                     locales={locales}
                     branding={branding}
                     features={features}
                     defaultLanguage={config.DEFAULT_LANGUAGE}
            />
        );
    }
}

export default App;
