# Pages and Sections

- pages are one url access point which combine everything for one exact view 
- sections are groups of pages which use the same parental structure

# Simple Pages
 
Pages which are very simple, are in root.

- `/Auth.js` login and authentification page


# Section Pages

Pages within a section receive render props for the same parental structure components to be able to fill them from below.

Are also in root, have a folder with their name in which the child pages are.