import React from 'react';
import {withNamespaces} from 'react-i18next';
import {useTheme, createUseStyles} from '@bemit/flood-admin/lib/theme';
import {MdMailOutline} from 'react-icons/md';

import StyleGrid from '@bemit/flood-admin/lib/StyleGrid';
import flood_info from '@bemit/flood-admin/lib/Info';

const useStyles = createUseStyles({
    wrapper: {
        display: 'table',
        tableLayout: 'fixed',
        width: '100%',
        padding: '6px 0 18px',
    },
    wrapperThin: {
        maxWidth: '560px',
        margin: '0 auto',
    },
    group: {
        display: 'table-row'
    },
    item: {
        textAlign: 'center',
        '&:first-child': {
            paddingLeft: 0,
        },
        '&:last-child': {
            paddingRight: 0,
        },
        [StyleGrid.smUp]: {
            display: 'table-cell',
            textAlign: 'left',
            padding: '2px 6px',
        }
    },
    rowItem: {
        captionSide: 'top',
        display: 'table-caption',
        textDecoration: 'underline',
        margin: '6px 0',
        textAlign: 'center'
    }
});

const Info = (props) => {
    const {t,} = props;
    const theme = useTheme();
    const classes = useStyles(theme);

    return (
        <React.Fragment>
            <h1>{t('title')}</h1>
            <p>{t('intro')}</p>
            <div className={classes.wrapper + ' ' + classes.wrapperThin}>
                <p className={classes.rowItem}>{t('info.title')}</p>
                <div className={classes.group}>
                    <p className={classes.item}>{t('info.variant')}</p>
                    <p className={classes.item}>Hydro\Admin <i>v{process.env.REACT_APP_VERSION}</i> <a href='https://bemit-eu.atlassian.net/projects/EFHADM/issues' target='_blank' rel='noopener noreferrer'>Issues</a></p>
                </div>
                <div className={classes.group}>
                    <p className={classes.item}>{t('info.base')}</p>
                    <p className={classes.item}>Flood\Admin <i>v{flood_info.version}</i> <a href='https://bemit-eu.atlassian.net/projects/EFDADM/issues' target='_blank' rel='noopener noreferrer'>Issues</a></p>
                </div>
            </div>

            <div className={classes.wrapper + ' ' + classes.wrapperThin}>
                <p className={classes.rowItem}>{t('info.support')}</p>
                <div className={classes.group}>
                    <p className={classes.item}>{t('info.company')}</p>
                    <p className={classes.item}>bemit UG (haftungsbeschränkt)<br/>Langgasse 29<br/>55283 Nierstein<br/>Handelsregister: 47413, Mainz<br/>CEO: Michael Becker</p>
                </div>
                <div className={classes.group}>
                    <p className={classes.item}>{t('info.contact.support.title')}</p>
                    <p className={classes.item}>
                        <a href='https://help.flood.bemit.codes' target='_blank' rel='noopener noreferrer'>{t('info.contact.support.link')}</a><br/>
                        <a href={'mailto:support@bemit.codes?subject=' + encodeURI(t('info.contact.support.subject'))}>
                            <MdMailOutline color={theme.textColor} size={'1.125em'}/> {t('info.contact.support.request')}</a>
                    </p>
                </div>
                <div className={classes.group}>
                    <p className={classes.item}>{t('info.contact.guide.title')}</p>
                    <p className={classes.item}>
                        <a href='https://help.flood.bemit.codes/admin/guide' target='_blank' rel='noopener noreferrer'>{t('info.contact.guide.link')}</a><br/>
                        <a href={'mailto:support@bemit.codes?subject=' + encodeURI(t('info.contact.guide.subject'))}>
                            <MdMailOutline color={theme.textColor} size={'1.125em'}/> {t('info.contact.guide.request')}</a>
                    </p>
                </div>
                <div className={classes.group}>
                    <p className={classes.item}>{t('info.contact.law.title')}</p>
                    <p className={classes.item}>
                        <i>{t('info.contact.law.nosupport')}</i><br/>
                        <a href='tel:004915162412375'>+49 151 624 132 75</a><br/>
                        <a href={'mailto:contact@bemit.eu?subject=' + encodeURI(t('info.contact.law.subject'))}>
                            <MdMailOutline color={theme.textColor} size={'1.125em'}/> contact@bemit.eu</a>
                    </p>
                </div>
            </div>

            <div className={classes.wrapper}>
                <p className={classes.rowItem}>{t('info.licence.title')}</p>
                <div className={classes.group}>
                    <div className={classes.item}>
                        <h3>Flood</h3>
                        <p>Copyright 2015 - 2019</p>
                        <p>bemit UG (haftungsbeschränkt)</p>

                        <h3>Flood\Hydro: Admin</h3>
                        <p>Copyright 2018 - 2019</p>
                        <p>bemit UG (haftungsbeschränkt)</p>
                        <p>This project is free software distributed under the terms of two licences, the CeCILL-C and the GNU Lesser General Public License. You can use, modify and/ or redistribute the software under the terms of
                            CeCILL-C
                            (v1) for Europe or GNU LGPL (v3) for the rest of the world.</p>
                        <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General
                            Public
                            License for more details.</p>
                        <p> Please see original files:<br/>
                            <a href='https://bitbucket.org/bemit_eu/hydro-admin/src/master/LICENCE.md' target='_blank' rel='noopener noreferrer'>Licence</a><br/>
                            <a href='https://bitbucket.org/bemit_eu/hydro-admin/src/master/LICENCE.cecillc' target='_blank' rel='noopener noreferrer'>CeCILL-C</a><br/>
                            <a href='https://bitbucket.org/bemit_eu/hydro-admin/src/master/LICENCE.lgpl' target='_blank' rel='noopener noreferrer'>LPGL</a>
                        </p>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default withNamespaces('page--info')(Info);
